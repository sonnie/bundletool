Document: bundletool
Title: Debian bundletool Manual
Author: <insert document author here>
Abstract: This manual describes what bundletool is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/bundletool/bundletool.sgml.gz

Format: postscript
Files: /usr/share/doc/bundletool/bundletool.ps.gz

Format: text
Files: /usr/share/doc/bundletool/bundletool.text.gz

Format: HTML
Index: /usr/share/doc/bundletool/html/index.html
Files: /usr/share/doc/bundletool/html/*.html
