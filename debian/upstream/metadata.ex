# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/bundletool/issues
# Bug-Submit: https://github.com/<user>/bundletool/issues/new
# Changelog: https://github.com/<user>/bundletool/blob/master/CHANGES
# Documentation: https://github.com/<user>/bundletool/wiki
# Repository-Browse: https://github.com/<user>/bundletool
# Repository: https://github.com/<user>/bundletool.git
